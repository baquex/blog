import {
	UPDATE_TITLE, 
	UPDATE_AUTHOR,
	UPDATE_EXCERPT,
	UPDATE_CONTENT,
	UPDATE_CATEGORY,
	UPDATE_CREATED, 
	UPDATE_UPDATED,
	ADD_ARTICLE
} from '../actions/actionTypes';

const INITIAL_STATE = {
	posts: {
		id: '',
		title: '',
		author: '',
		excerpt: '',
		content: '',
		category: '',
		created: '',
		updated: ''
	},
	postList: []
};


export default (state = INITIAL_STATE, action) =>{
	switch (action.type) {
			case UPDATE_TITLE:
					return updateTitle(state,action);
			case UPDATE_AUTHOR:
					return updateAuthor(state,action);
			case UPDATE_EXCERPT:
					return updateExcerpt(state,action);
			case UPDATE_CONTENT:
					return updateContent(state,action);
			case UPDATE_CATEGORY:
					return updateCategory(state,action);
			case UPDATE_CREATED:
					return updateCreated(state,action);
			case UPDATE_UPDATED:
					return updateUpdated(state,action);
			case ADD_ARTICLE:
					return addArticle(state);
			default: 
          return state
    }
}

function updateTitle(state,action) {
	let txt = action.payload;	
	return {...state, posts: {
		id: state.postList.length,
		title: txt,
		author: state.posts.author,
		excerpt: state.posts.excerpt,
		content: state.posts.content,
		category: state.posts.category,
		created: state.posts.created,
		updated: state.posts.updated
	}}
}

function updateAuthor(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: txt,
		excerpt: state.posts.excerpt,
		content: state.posts.content,
		category: state.posts.category,
		created: state.posts.created,
		updated: state.posts.updated
	}}
}

function updateExcerpt(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: state.posts.author,
		excerpt: txt,
		content: state.posts.content,
		category: state.posts.category,
		created: state.posts.created,
		updated: state.posts.updated
	}}
}

function updateContent(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: state.posts.author,
		excerpt: state.posts.excerpt,
		content: txt,
		category: state.posts.category,
		created: state.posts.created,
		updated: state.posts.updated
	}}
}

function updateCategory(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: state.posts.author,
		excerpt: state.posts.excerpt,
		content: state.posts.content,
		category: txt,
		created: state.posts.created,
		updated: state.posts.updated
	}}
}

function updateCreated(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: state.posts.author,
		excerpt: state.posts.excerpt,
		content: state.posts.content,
		category: state.posts.category,
		created: txt,
		updated: state.posts.updated
	}}
}

function updateUpdated(state,action) {
	let txt = action.payload;
	return {...state, posts: {
		id: state.postList.length,
		title: state.posts.title,
		author: state.posts.author,
		excerpt: state.posts.excerpt,
		content: state.posts.content,
		category: state.posts.category,
		created: state.posts.created,
		updated: txt
	}}
}

function addArticle(state){
	let postList = [...state.postList];
	console.log(state.posts);
	postList.push(state.posts);
	console.log(state.postList);
	
	return {...state, posts: {
		id: '',
		title: '',
		author: '',
		excerpt: '',
		content: '',
		category: '',
		created: '',
		updated: ''}, postList};
}
