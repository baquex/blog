import {combineReducers} from 'redux';
import blogReducers from './blog_reducers';

export default combineReducers({
    blogReducers
})