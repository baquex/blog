export const UPDATE_TITLE = 'update_title';
export const UPDATE_AUTHOR = 'update_author';
export const UPDATE_EXCERPT = 'update_excerpt';
export const UPDATE_CONTENT = 'update_content';
export const UPDATE_CATEGORY = 'update_category';
export const UPDATE_CREATED = 'update_created';
export const UPDATE_UPDATED = 'update_updated';
export const ADD_ARTICLE = 'add_article';