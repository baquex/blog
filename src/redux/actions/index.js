import {
		UPDATE_TITLE, 
		UPDATE_AUTHOR,
		UPDATE_EXCERPT,
		UPDATE_CONTENT,
		UPDATE_CATEGORY, 
		UPDATE_CREATED, 
		UPDATE_UPDATED,
		ADD_ARTICLE
} from './actionTypes';

export const updateTitle = title =>{
    return {
        type: UPDATE_TITLE,
        payload: title
    }
}

export const updateAuthor = author =>{
    return {
        type: UPDATE_AUTHOR,
        payload: author
    }
}

export const updateExcerpt = excerpt =>{
    return {
        type: UPDATE_EXCERPT,
        payload: excerpt
    }
}

export const updateContent = content =>{
    return {
        type: UPDATE_CONTENT,
        payload: content
    }
}

export const updateCategory = category =>{
    return {
        type: UPDATE_CATEGORY,
        payload: category
    }
}

export const updateCreated = created =>{
    return {
        type: UPDATE_CREATED,
        payload: created
    }
}

export const updateUpdated = updated =>{
    return {
        type: UPDATE_UPDATED,
        payload: updated
    }
}

export const addArticle = () =>{
    return {
        type: ADD_ARTICLE,
        payload: ''
    }
}
