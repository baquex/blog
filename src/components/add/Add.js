import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';
import * as actions from '../../redux/actions';
import {Link} from 'react-router-dom';

const Add = props =>{
	
	return (
	<div className="container">
		<div className="row">
			<div className="col-md-6 offset-md-3">
				<h3 className="my-3">Add a new article:</h3>
			</div>
		</div>
		<div className="row d-flex">
			<div className="col-md-6 offset-md-3 myform mb-3">
				<div className="form">
					<label className="col-form-label">Article Title</label>
					<div>
						<input className="form-control" type="text" id="title-input" value={props.posts.title} onChange={event => props.updateTitle(event.target.value)}/>
					</div>
					<label className="col-form-label">Article Author</label>
					<div>
						<input className="form-control" type="text" id="author-input" value={props.posts.author} onChange={event => props.updateAuthor(event.target.value)}/>
					</div>
					<label className="col-form-label">Article Excerpt</label>
					<div>
						<textarea className="form-control" rows="3" id="excerpt-input" value={props.posts.excerpt} onChange={event => props.updateExcerpt(event.target.value)}/>
					</div>
					<label className="col-form-label">Article Content</label>
					<div>
						<textarea className="form-control" rows="5" id="content-input" value={props.posts.content} onChange={event => props.updateContent(event.target.value)}/>
					</div>
					<label className="col-form-label">Article Category</label>
					<div>
						<input className="form-control" type="text" id="category-input" value={props.posts.category} onChange={event => props.updateCategory(event.target.value)}/>
					</div>
					<div className="row">
						<div className="col-6">
							<label className="col-form-label mx-2">Date Created</label>
							<input className="form-control" type="date" id="date-created" value={props.posts.created} onChange={event => props.updateCreated(event.target.value)}/>
						</div>
						<div className="col-6">
							<label className="col-form-label mx-2">Date Updated</label>
							<input className="form-control" type="date" id="date-updated" value={props.posts.updated} onChange={event => props.updateUpdated(event.target.value)}/>
						</div>
					</div>
					<div className="row">
						<div className="col">
							<div className="d-flex justify-content-center my-3">
								<Link to="/" onClick={props.addArticle} className="p-2 border border-primary bg-light">Save</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
)
}
const mapStateToProps = ({blogReducers}) =>{
     const {posts} = blogReducers;
     return {posts}
}

export default connect(mapStateToProps, actions)(Add);