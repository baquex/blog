import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Add from './add/Add';
import Home from './Home/Home';

const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/add' component={Add} />
        </Switch>
    </main>
)

export default Main;