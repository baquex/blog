import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../index.css';
import {Link} from 'react-router-dom';

const Header = () => (
    <div className="container-fluid">
			<div className="row">
				<div className="col-12 bg-dark myheader d-flex align-items-center">
					<ul className="nav-bar nav">
							<li className="nav-item mr-5">
									<Link to="/">Home</Link>
							</li>
							<li className="nav-item">
									<Link to="/add">Add</Link>
							</li>
					</ul>
				</div>
			</div>
    </div>
)

export default Header;