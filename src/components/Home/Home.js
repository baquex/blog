import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../redux/actions';
import {
  Container,
  Row,
  Col,
  Card,
	CardBody,
	CardTitle,
	CardText,
	CardSubtitle,
	Button
  } from 'reactstrap';

const Home = (props) => { 

	let tempList = props.postList;
	let listaPosts = tempList.map((item,index) =>{
	return (
		<Container className="mt-3">
			<Row>
				<Col md={{size: 6, offset: 1}}>
					<Card>
						<CardBody>
							<CardTitle><h2>{item.title}</h2></CardTitle>
							<CardSubtitle className="text-muted">Author: {item.author}</CardSubtitle>
							<CardText>{item.excerpt}</CardText>
							<Button>Read More</Button>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</Container>
	)
	})

	if (!tempList.length){
		return (
			<Container className="mt-3">
				<Row>
					<Col md={{size: 8, offset: 2}}>
						<Card className="text-center bg-secondary">
							<CardBody>
								<h2>There are no posts to show</h2>
							</CardBody>
						</Card>
					</Col>
				</Row>
			</Container>
		)
	}
	else
		return (<div>{listaPosts}</div>)
};


const mapStateToProps = ({blogReducers}) =>{
    const {postList} = blogReducers;
    return {postList}
}

export default connect(mapStateToProps, actions)(Home);